﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class HistoryGenerator
    {
        Dictionary<Color, ProvinceHistory> provinces = new Dictionary<Color, ProvinceHistory>();

        /// <summary>
        /// Will generate all history files for the generator. 
        /// </summary>
        /// <param name="tags"></param>
        public void GenerateHistory(List<Tag> tags)
        {
            LoadProvinces();
            CreateAdjacencies();

            CreateHistory(tags);
            


            //write each provinces history. 
            for (int idx = 0; idx < provinces.Count; idx++)
            {
                provinces.ElementAt(idx).Value.writeHistory();
            }
           

        }

        /// <summary>
        /// loads the provinces into the history generator. 
        /// </summary>
        protected void LoadProvinces()
        {
            Dictionary<int, string> provinceNames = new Dictionary<int, string>();
            int idxLength;
            string[] provinceFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/history/provinces");
            foreach (string filename in provinceFileNames)
            {
                string filenameWithoutPath = Path.GetFileNameWithoutExtension(filename);
                File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath + ".txt");
               // provinceName = filenameWithoutPath;
                if (filenameWithoutPath.Contains("-"))
                {
                    idxLength = filenameWithoutPath.IndexOf("-");
                }
                else
                {
                    idxLength = filenameWithoutPath.IndexOf(" ");
                }

                provinceNames.Add(Convert.ToInt32(filenameWithoutPath.Substring(0, idxLength)),filenameWithoutPath.Substring(idxLength + 1));
            }

            StreamReader readFile;

            try
            {
                readFile = new StreamReader(Constants.MAP + "/definition.csv");
            }
            catch
            {
                readFile = new StreamReader(Constants.INSTALLPATH + "/map/definition.csv");
            }
            string line;
            while ((line = readFile.ReadLine()) != null)
            {
                int nextSemi = line.IndexOf(";");
                string provinceNo = line.Substring(0, nextSemi);
                int previousSemi = nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string red = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string green = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string blue = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                if (line.Substring(previousSemi).Contains(";"))
                {
                    nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                }
                else
                {
                    nextSemi =  line.Length - 1 -  previousSemi ;    
                }
                string provinceName = line.Substring(previousSemi, nextSemi);

                //make sure that all the files work properly by erasing the old files.  
                try
                {
                    provinceName = provinceNames[Convert.ToInt32(provinceNo)];
                }
                catch
                {
                    
                }

                try
                {
                    
                    ProvinceHistory province = new ProvinceHistory(Convert.ToInt32(provinceNo),
                        Color.FromArgb(Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue)),
                        provinceName);
                    provinces.Add(province.provinceColor, province);
                    Console.WriteLine("Added province " + provinceNo + " to history generator");
                }
                catch
                {
                    Console.WriteLine(provinceNo + " is not valid in the definitions file"); 
                }
            }
            Console.WriteLine("loaded all provinces to history generator");

        }

        /// <summary>
        /// create the adjacencies map, so the generator knows which province is next to which. 
        /// </summary>
        protected void CreateAdjacencies()
        {
            Bitmap provinceMap;

            try
            {
                provinceMap = new Bitmap(Constants.MAP + "/provinces.bmp");
            }
            catch
            {
                provinceMap = new Bitmap(Constants.INSTALLPATH + "/map/provinces.bmp");
            }
            Color previousColor = provinceMap.GetPixel(0, 0);

            for (int idx = 0; idx < provinceMap.Width; idx++)
            {
                for (int jdx = 0; jdx < provinceMap.Height; jdx++)
                {
                    if (provinceMap.GetPixel(idx, jdx).ToArgb() != previousColor.ToArgb())
                    {
                        //need to check 
                        provinces[provinceMap.GetPixel(idx, jdx)].AddAdjacency(provinces[previousColor]);
                        provinces[previousColor].AddAdjacency(provinces[provinceMap.GetPixel(idx, jdx)]);

                        previousColor = provinceMap.GetPixel(idx, jdx);
                       // GetProvinceFromColor(provinceMap.GetPixel(idx, jdx)).AddAdjacency(GetProvinceFromColor(previousColor));
                       // GetProvinceFromColor(previousColor).AddAdjacency(GetProvinceFromColor(provinceMap.GetPixel(idx, jdx)));
                    }
                }
                Console.WriteLine("Checked line " + idx + " for adjacenies");
            }
        }


        class HistoryTag
        {
            public Tag tag;
            public int yearsTillNextUpdate;
            public Color capitalColor;
        }
        /// <summary>
        /// creates the history itself. 
        /// </summary>
        protected void CreateHistory(List<Tag> tags)
        {
            //adds the owners and their capitals to the history list. 
            List<HistoryTag> tagHistory = new List<HistoryTag>();
            for (int idx = 0; idx < provinces.Count; idx++)
            {
                foreach (Tag tag in tags)
                {
                    if (provinces.ElementAt(idx).Value.provinceNo == tag.capitalProvince)
                    {
                        provinces.ElementAt(idx).Value.AddOwner(Constants.STARTDATE, tag.Id, tag.countryCulture);
                        
                        //Add all tags to a list of structs, storing info needed during the history generation.
                        HistoryTag t = new HistoryTag();
                        t.tag = tag;
                        t.yearsTillNextUpdate = 1;
                        t.capitalColor = provinces.ElementAt(idx).Key;
                        tagHistory.Add(t);
                    }
                }
            }

             
            

            int date = 1; 
            int finishDate = Random.RandomInt(Constants.STARTDATE + 100, Constants.ENDDATE);
            //varibles that change history. 
            int maxYearsBetweenExpansion = 10;
            int minYearsBetweenExpansion = 5;
            int recursionLimit = 100;

            while (date < finishDate)
            {
                for (int idx = 0; idx < tagHistory.Count; idx++)
                {
                    if (tagHistory[idx].yearsTillNextUpdate < 1)
                    {
                        //creates colonies. 
                        ProvinceHistory currentProvince = provinces[tagHistory[idx].capitalColor];
                        bool huntingForColony = true;
                        currentProvince = currentProvince.GetRandomAdjacency();
                        int recursionTimes = 0; 

                        while (huntingForColony)
                        {
                            if (recursionTimes > recursionLimit) // limits crossing forbidden provinces. 
                            {
                                huntingForColony = false;
                            }
                            else if (currentProvince.forbidden) //if the province is not allowed to be colonized. 
                            {
                                currentProvince.GetRandomAdjacency();
                                recursionTimes++;
                            }
                            else if (currentProvince.ownershipHistory.Count == 0) //if the province is empty, colonize. 
                            {
                                currentProvince.AddOwner(date, tagHistory[idx].tag.Id, tagHistory[idx].tag.countryCulture);
                                huntingForColony = false;
                            }
                            else if (currentProvince.ownershipHistory[currentProvince.ownershipHistory.Count-1].TAG == tagHistory[idx].tag.Id)
                            {
                                //if it is our province, look for another adjacency. 
                                currentProvince = currentProvince.GetRandomAdjacency();
                            }
                            else
                            {
                                //else, stop hunting. 
                                huntingForColony = false;
                            }
                        }


                        tagHistory[idx].yearsTillNextUpdate = Random.RandomInt(minYearsBetweenExpansion, maxYearsBetweenExpansion);
                    }
                    else
                    { 
                        tagHistory[idx].yearsTillNextUpdate--;
                    }
                }


                date++;
            }
        }

       /* protected ProvinceHistory GetProvinceFromColor(Color checkColor)
        {
            foreach (ProvinceHistory province in provinces)
            {
                if (province.provinceColor.ToArgb() == checkColor.ToArgb())
                {
                    return province;
                }
            }

            return null;
        }*/
    }

    class ProvinceHistory
    {
        public struct owners
        {
            public int takeoverYear;
            public string TAG;
            public string culture;
        }

        public int provinceNo { get; private set; }
        public Color provinceColor { get; private set; }
        public string provinceName { get; private set; }
        List<ProvinceHistory> Adjacencies = new List<ProvinceHistory>();

        public List<owners> ownershipHistory { get; private set;}

        /// <summary>
        /// whether this province can be settled or not. 
        /// </summary>
        public bool forbidden { get; private set; }

        public ProvinceHistory(int provinceNo, Color provinceColor, string provinceName)
        {
            this.provinceNo = provinceNo;
            this.provinceColor = provinceColor;
            this.provinceName = provinceName;

            forbidden = CapitalAssignment.forbidden(provinceNo);

            ownershipHistory = new List<owners>();
        }
        public void AddAdjacency(ProvinceHistory otherprovince)
        {
           /* bool alreadyRecorded = false;
            int idx = 0;
            while (!alreadyRecorded && (idx < Adjacencies.Count))
            {
                if (Adjacencies[idx].provinceNo == otherprovince.provinceNo)
                {
                    alreadyRecorded = true;
                    
                }
                idx++;
            }*/

            if (!Adjacencies.Contains(otherprovince))
            {
                Adjacencies.Add(otherprovince);
                Console.WriteLine(provinceNo + "is adjacent to " + otherprovince.provinceNo);
            }
        }

        public void AddOwner(int date, string tag, string culture)
        {
            if (!forbidden)
            {
                owners newOwner;
                newOwner.TAG = tag;
                newOwner.takeoverYear = date;
                newOwner.culture = culture;
                ownershipHistory.Add(newOwner);
                Console.WriteLine("added owner " + tag + " to province " + provinceName);
            }
        }

        public void writeHistory()
        {

            StreamWriter file;
            //file = File.CreateText(Constants.HISTORY + "/provinces/" + provinceName);
            Console.WriteLine("creating province " + provinceName + "'s history file");
            file = File.CreateText(Constants.HISTORY + "/provinces/" + provinceNo + "-" + provinceName + ".txt");

            if (!forbidden)
            {
                //for (int jdx = 0; jdx < tags.Count; jdx++)
                //{
                  //  if (provinceNo == tags[jdx].capitalProvince)
                    //{
                if (ownershipHistory.Count > 0)
                {
                    if (ownershipHistory[0].takeoverYear == Constants.STARTDATE)
                    {
                        file.WriteLine("add_core = " + ownershipHistory[0].TAG);
                        file.WriteLine("owner = " + ownershipHistory[0].TAG);
                        file.WriteLine("controller = " + ownershipHistory[0].TAG);
                        //file.WriteLine("1444.1.1 = { owner = " + tags[jdx].Id
                        //  + " controller = " + tags[jdx].Id + "}");
                        file.WriteLine("culture = " + ownershipHistory[0].culture);
                        file.WriteLine("fort1 = yes");   
                        file.WriteLine("is_city = yes");
                        
                        file.WriteLine("base_tax = 20");
                    }
                    else
                    {
                        file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                    }   
                    //file.WriteLine("religion = catholic");
                        
                    

                    

                 }
                else
                {
                    file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                }  
                //}

                file.WriteLine("religion = noreligion");

                file.WriteLine("capital = " + NameGenerator.GenerateName());
                file.WriteLine("hre = no");
                //file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                file.WriteLine("manpower = " + Random.RandomInt(1, 10));
                file.WriteLine("trade_goods = unknown");
                //file.WriteLine("discovered_by = western");
                

                int coreTime = 25;
                //foreach (owners owner in ownershipHistory) 
                for (int idx = 0; idx < ownershipHistory.Count; idx++)
                {
                    owners owner = ownershipHistory[idx];
                    file.WriteLine(owner.takeoverYear + ".2.2 = { owner = " + owner.TAG + " controller = " + owner.TAG + " }");
                    //if the first in the list. 
                    if (idx == 0)
                    {
                        file.WriteLine(owner.takeoverYear + ".2.3 = { culture = " + owner.culture + " }");
                    }
                    //if the last one in the list. 
                    if (idx + 1 == ownershipHistory.Count)
                    {
                        file.WriteLine((owner.takeoverYear + coreTime) + ".2.2 = { add_core = " + owner.TAG + " }");
                    }
                        //or if the time to next take over is too long. 
                    else if (owner.takeoverYear + coreTime < ownershipHistory[idx+1].takeoverYear)
                    {
                        file.WriteLine((owner.takeoverYear + coreTime) + ".2.2 = { add_core = " + owner.TAG + " }");
                    }
                    


                    file.Flush();
                }
            }
            file.Flush();
            file.Close();
        }

        /// <summary>
        /// returns a random adjacent province. 
        /// </summary>
        /// <returns></returns>
        public ProvinceHistory GetRandomAdjacency()
        {
            return Adjacencies[Random.RandomInt(0, Adjacencies.Count - 1)];
        }
    }
}
