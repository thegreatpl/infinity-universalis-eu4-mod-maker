﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class Decisions
    {
        public Decisions()
        {
            StreamWriter tribal = File.CreateText(Constants.DECISIONS + "/IUTribal.txt");

            Console.WriteLine("Creating Tribal Decisions");

          tribal.WriteLine("country_decisions = {");
	
	      tribal.WriteLine("    tribal_despotism_reform = {");
            tribal.WriteLine("      		major = yes");
            tribal.WriteLine("      		potential = {");
            tribal.WriteLine("			government = tribal_despotism");
            tribal.WriteLine("  		}");
            tribal.WriteLine("		allow = {");
            tribal.WriteLine("			adm_power = 200");
            tribal.WriteLine("  			legitimacy = 90");
			tribal.WriteLine(" stability = 3");
            tribal.WriteLine("          OR = {");
            tribal.WriteLine("              num_of_cities = 10");
            tribal.WriteLine("              is_year = " + (Constants.STARTDATE + 25));
            tribal.WriteLine("          }");
            tribal.WriteLine("  		}");
            tribal.WriteLine("		effect = {");
            tribal.WriteLine("			change_government = despotic_monarchy");
            tribal.WriteLine("			add_stability = -5");
            tribal.WriteLine("			add_adm_power = -200");
            tribal.WriteLine("		}");
            tribal.WriteLine("		ai_will_do = {");
            tribal.WriteLine("			factor = 1");
            tribal.WriteLine("		}");
            tribal.WriteLine("	}");

            tribal.WriteLine("    tribal_federation_reform = {");
            tribal.WriteLine("      		major = yes");
            tribal.WriteLine("      		potential = {");
            tribal.WriteLine("			government = tribal_federation");
            tribal.WriteLine("  		}");
            tribal.WriteLine("		allow = {");
            tribal.WriteLine("			adm_power = 200");
            tribal.WriteLine("  			legitimacy = 90");
			tribal.WriteLine(" stability = 3");
            tribal.WriteLine("          OR = {");
            tribal.WriteLine("              num_of_cities = 10");
            tribal.WriteLine("              is_year = " + (Constants.STARTDATE + 25));
            tribal.WriteLine("          }");
            tribal.WriteLine("  		}");
            tribal.WriteLine("		effect = {");
            tribal.WriteLine("			change_government = feudal_monarchy");
            tribal.WriteLine("			add_stability = -5");
            tribal.WriteLine("			add_adm_power = -200");
            tribal.WriteLine("		}");
            tribal.WriteLine("		ai_will_do = {");
            tribal.WriteLine("			factor = 1");
            tribal.WriteLine("		}");
            tribal.WriteLine("	}");

            tribal.WriteLine("}");

            tribal.Flush();
            tribal.Close();
        }
    }
}
