﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EuropaUniversalis4ModMaker
{
    static class CapitalAssignment
    {
        static List<int> forbiddenProvinces = new List<int>();
        static public int totalProvinces { get; private set;  }

        /// <summary>
        /// inits the various values of the class. 
        /// </summary>
        static public void Init()
        {
            try
            {
                System.IO.StreamReader read = new System.IO.StreamReader(Constants.MAP + "/default.map");
                string line; 
                bool sea = false;

                

                while ((line = read.ReadLine()) != null)
                {
                    if (line.Contains("max_provinces"))
                    {
                        totalProvinces = Convert.ToInt32(line.Substring(line.IndexOf("=") + 1));
                    }

                    if (line.Contains("}"))
                    {
                        sea = false;
                    }
                    if (line.Contains("sea_starts") || line.Contains("lakes"))
                    {
                        sea = true;
                    }

                    if (sea)
                    {
                        string[] filecontents = (line.Split(' '));
                        for (int idx = 1; idx < filecontents.Length; idx++)
                        {
                            try
                            {
                                forbiddenProvinces.Add(Convert.ToInt32(filecontents[idx]));
                            }
                            catch
                            {
                                Console.WriteLine("ERROR: not a number");
                            }
                        }
                    }
                }
                read.Close();
            }
            catch
            {
                
                try
                {
                    System.IO.StreamReader read2 = new System.IO.StreamReader(Constants.INSTALLPATH + "/map/default.map");
                    string line;
                    bool sea = false;



                    while ((line = read2.ReadLine()) != null)
                    {
                        if (line.Contains("max_provinces"))
                        {
                            string testing = line.Substring(line.IndexOf("=") + 1);
                            totalProvinces = Convert.ToInt32(testing);
                        }
                        if (line.Contains("}"))
                        {
                            sea = false;
                        }
                        if (line.Contains("sea_starts") || line.Contains("lakes"))
                        {
                            sea = true;
                        }

                        if (sea)
                        {
                            string[] filecontents2 = (line.Split(' '));
                            for (int idx = 1; idx < filecontents2.Length; idx++)
                            {
                                try
                                {
                                    forbiddenProvinces.Add(Convert.ToInt32(filecontents2[idx]));
                                }
                                catch
                                {
                                    Console.WriteLine("ERROR: not a number");
                                }
                            }
                        }
                    }
                    read2.Close();
                }
                catch
                {
                    System.IO.StreamReader read = new System.IO.StreamReader(@"ForbiddenProvinces.txt");
                    string[] filecontents = (read.ReadToEnd().Split(' '));
                    for (int idx = 1; idx < filecontents.Length; idx++)
                    {
                        try
                        {
                            if (filecontents[idx].Contains("/"))
                            {
                                filecontents[idx] = filecontents[idx].Substring(0, filecontents[idx].IndexOf("/"));
                            }
                            if (!(filecontents[idx][0].Equals("/")))
                            {
                                forbiddenProvinces.Add(Convert.ToInt32(filecontents[idx]));
                            }
                        }
                        catch
                        {
                            Console.WriteLine("ERROR: not a number");
                        }
                    }

                    totalProvinces = 2002; 
                }

            }

            try
            {
                System.IO.StreamReader read = new System.IO.StreamReader(Constants.MAP + "/climate.txt");
                string line;
                bool impassable = false;
                while ((line = read.ReadLine()) != null)
                {
                    if (line.Contains("impassable"))
                    {
                        impassable = true;
                    }

                    if (line.Contains("}"))
                    {
                        impassable = false;
                    }

                    if (impassable)
                    {
                        string[] filecontents = (line.Split(' '));
                        for (int idx = 1; idx < filecontents.Length; idx++)
                        {
                            try
                            {
                                forbiddenProvinces.Add(Convert.ToInt32(filecontents[idx]));
                            }
                            catch
                            {
                                Console.WriteLine("ERROR: not a number");
                            }
                        }
                    }
                }
            }
            catch
            {
                System.IO.StreamReader read = new System.IO.StreamReader(Constants.INSTALLPATH + "/map/climate.txt");
                string line;
                bool impassable = false;
                while ((line = read.ReadLine()) != null)
                {
                    if (line.Contains("impassable"))
                    {
                        impassable = true;
                    }

                    if (line.Contains("}"))
                    {
                        impassable = false;
                    }

                    if (impassable)
                    {
                        string[] filecontents = (line.Split(' '));
                        for (int idx = 1; idx < filecontents.Length; idx++)
                        {
                            try
                            {
                                forbiddenProvinces.Add(Convert.ToInt32(filecontents[idx]));
                            }
                            catch
                            {
                                Console.WriteLine("ERROR: not a number");
                            }
                        }
                    }
                }
                Console.WriteLine("could not find a climate file for the mod");
            }


            Console.WriteLine("Loaded " + forbiddenProvinces.Count + " forbiddenProvinces");
        }

        static public void Init(Map map)
        {
            foreach (Province province in map.seaProvinceList)
            {
                forbiddenProvinces.Add(province.provinceNo);
            }
            totalProvinces = map.provinceList.Count;
            

        }

        /// <summary>
        /// returns a province to be assigned as a capital province. 
        /// </summary>
        /// <returns></returns>
        static public int AssignCapital()
        {
            int capital = Random.RandomInt(1, totalProvinces);

            if (forbidden(capital))
            {
                return AssignCapital();
            }
            else
            {
                return capital;
            }
        }

        /// <summary>
        /// returns whether or not the province is on the forbidden list. 
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        static public bool forbidden(int provinceId)
        {
            for (int idx = 0; idx < forbiddenProvinces.Count; idx++)
            {
                if (forbiddenProvinces[idx] == provinceId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
