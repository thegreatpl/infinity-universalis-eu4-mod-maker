﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace EuropaUniversalis4ModMaker
{
    static class NameGenerator
    {
        static List<string> Syllables = new List<string>();

        static public void Init()
        {
            System.IO.StreamReader read = new System.IO.StreamReader(@"Syllables.txt");

            int counter = 0;
            string line;

            while ((line = read.ReadLine()) != null)
            {
                Syllables.Add(line);
                counter++;
            }

            Console.WriteLine("Loaded " + counter + " Syllables into NameGenerator");

        }

        static public  string GenerateName()
        {
            return GenerateName(Random.RandomInt(2, 5));
        }
        static public  string GenerateName(int syllables)
        {
            string name = "";
            for (int idx = 0; idx < syllables; idx++)
            {
                name += GetSyllable();
            }
            name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name);
            return name;
        }

        static string GetSyllable()
        {
            return Syllables[Random.RandomInt(0, Syllables.Count)];
        }
    }
}
