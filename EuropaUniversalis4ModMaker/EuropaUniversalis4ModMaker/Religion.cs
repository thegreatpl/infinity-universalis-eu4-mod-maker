﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ImageMagick;
using System.Drawing;

namespace EuropaUniversalis4ModMaker
{
    class Religion
    {
        public List<List<string>> religions { protected set; get; }

        StreamWriter localization;

        /// <summary>
        /// Creates all religion related files and religions themselves. 
        /// </summary>
        public void CreateReligions()
        {
            localization = File.CreateText(Constants.LOCAL + "/IUReligions_l_english.yml");
            localization.WriteLine("l_english:");
            localization.Flush();
            religions = new List<List<string>>();

            int noOfBaseReligions = 30;

            for (int idx = 0; idx < noOfBaseReligions; idx++)
            {
                religions.Add(new List<string>());
                religions[idx].Add(NameGenerator.GenerateName());
                Console.WriteLine("created religion " + religions[idx][0]);

                //create schismatic religions. 
                for (int jdx = 0; jdx < Random.RandomInt(1, 9); jdx++)
                {
                    float percentage = Random.RandomFloat();

                    if (percentage > 0.9f)
                    {
                        religions[idx].Add("Reformed_" + religions[idx][0]);
                    }
                    else if (percentage > 0.8f)
                    {
                        religions[idx].Add("New_" + religions[idx][0]);
                    }
                    else
                    {
                        religions[idx].Add(NameGenerator.GenerateName());
                    }
                    Console.WriteLine("created religion " + religions[idx][jdx + 1]);
                }
            }

            CreateReligionsFile();
            CreateReligionFoundingEvents();
            CreateRelgiousIcons(); //is currently broken. 


            localization.Flush();
            localization.Close();
        }
        /// <summary>
        /// creates the religion file for the game. 
        /// </summary>
        protected void CreateReligionsFile()
        {
            StreamWriter output = File.CreateText(Constants.COMMON + "/religions/01_religion.txt");

            //StreamReader oldFile = new StreamReader(Constants.INSTALLPATH + "/common/religions/00_religion.txt");

            //string line;
            //while ((line = oldFile.ReadLine()) != null)
            //{
            //    output.WriteLine(line);
            //    output.Flush();
            //}

            //create noreligion
            output.WriteLine("athiest = { ");
            output.WriteLine("  noreligion = {");
            output.WriteLine("      color = { 0 0 0 }");
            output.WriteLine("      heretic = { ARGUMENTALISTS }");
            output.WriteLine("  }");
            output.WriteLine("  crusade_name = FUNDAMENTALIST");
            output.WriteLine("}");
            output.Flush();

            foreach (List<string> list in religions)
            {
                output.WriteLine(list[0] + "_group = {");

                foreach (string s in list)
                {
                    output.WriteLine("  " + s + " = {");
                    output.WriteLine("      color = { " + Random.RandomFloat() + " " + Random.RandomFloat() + " " + Random.RandomFloat(0, 0.9f) + " }");
                    output.WriteLine("      allowed_conversion = { ");
                    foreach (string o in list)
                    {
                        if (!(o == s))
                        {
                            output.WriteLine("          " + o);
                        }
                    }
                    output.WriteLine("      }");

                    string hereticNames = "";
                    for (int idx = 0; idx < Random.RandomInt(1, 5); idx++)
                    {
                        hereticNames += (" " + NameGenerator.GenerateName());
                    }

                    output.WriteLine("      heretic = { " + hereticNames + " }");
                    output.WriteLine("  }");
                    output.Flush();
                }

                string crusadeName = "CRUSADE";
                float percentile = Random.RandomFloat();
                if (percentile > 0.9f)
                {
                    crusadeName = "JIHAD";
                }
                else if (percentile > 0.5f)
                {
                    crusadeName = NameGenerator.GenerateName();
                }


                output.WriteLine("  crusade_name = " + crusadeName);
                output.WriteLine("}");
                output.WriteLine();

                output.Flush();
            }

            Console.WriteLine("Religion file written");
            output.Close();
        }

        /// <summary>
        /// Creates various events for the spreading and founding of religions. 
        /// </summary>
        protected void CreateReligionFoundingEvents()
        {
            StreamWriter output = File.CreateText(Constants.EVENTS + "/IUreligious.txt");
            string name = "IUreligious";
            output.WriteLine("namespace = " + name);
            int CurrentID = 1;

            //religion spread events. 
            foreach (List<string> list in religions)
            {
                foreach (string s in list)
                {
                    output.WriteLine("province_event = {");
                    output.WriteLine("  id = " + name + "." + CurrentID);
                    output.WriteLine("  title = " + '"' + name + ".NAME" + CurrentID + '"');
                    output.WriteLine("  desc = " + '"' + name + ".DESC" + CurrentID + '"');
                    output.WriteLine("  picture = RELIGION_eventPicture");
                    output.WriteLine("  trigger = {");
                    output.WriteLine("      religion = noreligion");
                    output.WriteLine("      any_neighbor_province = { religion = " + s + "}");
                    output.WriteLine("  }");
                    output.WriteLine("  mean_time_to_happen = {");
                    output.WriteLine("      months = " + Random.RandomInt(1, 36));
                    output.WriteLine("  }");
                    output.WriteLine("  option = {");
                    output.WriteLine("      name = " + '"' + name + ".EVE" + CurrentID + '"');
                    output.WriteLine("      change_religion = " + s);
                    output.WriteLine("  }");
                    output.WriteLine("}");
                    output.Flush();

                    //localize the event:
                    localization.WriteLine(" " + name + ".NAME:" + CurrentID + '"' + "Spread of " + s + '"');
                    localization.WriteLine(" " + name + ".DESC:" + CurrentID + '"' + s + " has spread into your province of $PROVINCENAME$" + '"');
                    localization.Flush();

                    CurrentID++;
                }
            }

            //religion founding events. 
            foreach (List<string> list in religions)
            {
                output.WriteLine("province_event = {");
                output.WriteLine("  id = " + name + "." + CurrentID);
                output.WriteLine("  title = " + '"' + name + ".NAME" + CurrentID + '"');
                output.WriteLine("  desc = " + '"' + name + ".DESC" + CurrentID + '"');
                output.WriteLine("  picture = RELIGION_eventPicture");
                output.WriteLine("  fire_only_once = yes");
                output.WriteLine("  major = yes");
                output.WriteLine("  trigger = {");
                output.WriteLine("      religion = noreligion");
                output.WriteLine("  }");
                output.WriteLine("  mean_time_to_happen = {");
                output.WriteLine("      months = " + Random.RandomInt(1, 36));
                output.WriteLine("  }");
                output.WriteLine("  option = {");
                output.WriteLine("      name = " + '"' + name + ".EVE" + CurrentID + '"');
                output.WriteLine("      change_religion = " + list[0]);
                output.WriteLine("  }");
                output.WriteLine("}");
                output.Flush();

                //localize the event:
                localization.WriteLine(" " + name + ".NAME:" + CurrentID + '"' + "Founding of " + list[0] + '"');
                localization.WriteLine(" " + name + ".DESC:" + CurrentID + '"' + list[0] + " has been founded in the province of $PROVINCENAME$" + '"');
                localization.Flush();

                CurrentID++;
            }

            //religious conversion events
            foreach (List<string> list in religions)
            {
                foreach (string s in list)
                {
                    output.WriteLine("country_event = {");
                    output.WriteLine("  id = " + name + "." + CurrentID);
                    output.WriteLine("  title = " + '"' + name + ".NAME" + CurrentID + '"');
                    output.WriteLine("  desc = " + '"' + name + ".DESC" + CurrentID + '"');
                    output.WriteLine("  picture = RELIGION_eventPicture");
                    output.WriteLine("  trigger = {");
                    output.WriteLine("      religion = noreligion");
                    output.WriteLine("      num_of_religion = { religion = " + s + " value = 0.5 }");
                    output.WriteLine("  }");
                    output.WriteLine("  mean_time_to_happen = {");
                    output.WriteLine("      months = " + Random.RandomInt(12, 60));
                    output.WriteLine("  }");
                    output.WriteLine("  option = {");
                    output.WriteLine("      name = " + '"' + name + ".EVE" + CurrentID + '"');
                    output.WriteLine("      change_religion = " + s);
                    output.WriteLine("  }");
                    output.WriteLine("}");
                    output.Flush();

                    localization.WriteLine(" " + name + ".NAME:" + CurrentID + '"' + "Spread of " + s + '"');
                    localization.WriteLine(" " + name + ".DESC:" + CurrentID + '"' + s + " has become the state religion of $COUNTRY$" + '"');
                    localization.Flush();

                    CurrentID++;
                }
            }

            //Schism formation events. 
            foreach (List<string> list in religions)
            {
                foreach (string s in list)
                {
                    if (s != list[0])
                    {
                        output.WriteLine("country_event = {");
                        output.WriteLine("  id = " + name + "." + CurrentID);
                        output.WriteLine("  title = " + '"' + name + ".NAME" + CurrentID + '"');
                        output.WriteLine("  desc = " + '"' + name + ".DESC" + CurrentID + '"');
                        output.WriteLine("  picture = RELIGION_eventPicture");
                        output.WriteLine("  fire_only_once = yes");
                        output.WriteLine("  major = yes");
                        output.WriteLine("  trigger = {");
                        output.WriteLine("      NOT = { is_religion_enabled = " + s + " }");
                        output.WriteLine("      OR = {");
                        foreach (string o in list)
                        {
                            if (s != o)
                            {
                                output.WriteLine("          religion = " + o);
                            }
                        }
                        output.WriteLine("      }");
                        output.WriteLine("      is_defender_of_faith = no");
                        output.WriteLine("      reform_desire = 0.95");
                        output.WriteLine("  }");
                        output.WriteLine("  mean_time_to_happen = {");
                        output.WriteLine("      months = " + Random.RandomInt(100, 2000));
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.75");
                        output.WriteLine("          reform_desire = 1.0");
                        output.WriteLine("      }");
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.75");
                        output.WriteLine("          reform_desire = 1.05");
                        output.WriteLine("      }");
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.75");
                        output.WriteLine("          reform_desire = 1.10");
                        output.WriteLine("      }");
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.66");
                        output.WriteLine("          reform_desire = 1.15");
                        output.WriteLine("      }");
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.50");
                        output.WriteLine("          reform_desire = 1.20");
                        output.WriteLine("      }");
                        output.WriteLine("      modifer = {");
                        output.WriteLine("          factor = 0.25");
                        output.WriteLine("          reform_desire = 1.25");
                        output.WriteLine("      }");
                        output.WriteLine("  }");
                        output.WriteLine("  option = {");
                        output.WriteLine("      name = " + '"' + name + ".EVE" + CurrentID + '"');
                        output.WriteLine("      enable_religion = " + s);
                        output.WriteLine("      random_owned_province = {");
                        output.WriteLine("          change_religion = " + s);
                        output.WriteLine("      }");
                        output.WriteLine("  }");
                        output.WriteLine("}");
                        output.Flush();

                        //localize the event:
                        localization.WriteLine(" " + name + ".NAME:" + CurrentID + '"' + "Founding of " + s + '"');
                        localization.WriteLine(" " + name + ".DESC:" + CurrentID + '"' + s + " has been founded in the country of $COUNTRY$. The local religion has schismed" + '"');
                        localization.Flush();


                        CurrentID++;
                    }
                }
            }

            //Schism spread events
            foreach (List<string> list in religions)
            {
                foreach (string s in list)
                {
                    output.WriteLine("province_event = {");
                    output.WriteLine("  id = " + name + "." + CurrentID);
                    output.WriteLine("  title = " + '"' + name + ".NAME" + CurrentID + '"');
                    output.WriteLine("  desc = " + '"' + name + ".DESC" + CurrentID + '"');
                    output.WriteLine("  picture = RELIGION_eventPicture");
                    output.WriteLine("  trigger = {");
                    output.WriteLine("      OR = {");
                    foreach (string o in list)
                    {
                        if (s != o)
                        {
                            output.WriteLine("          religion = " + o);
                        }
                    }
                    output.WriteLine("      }");
                    output.WriteLine("      any_neighbor_province = { religion = " + s + "}");
                    output.WriteLine("      owner = { reform_desire = 0.95 }");
                    output.WriteLine("      is_religion_enabled = " + s);
                    output.WriteLine("  }");
                    output.WriteLine("  mean_time_to_happen = {");
                    output.WriteLine("      months = " + Random.RandomInt(12, 60));
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.75");
                    output.WriteLine("          reform_desire = 1.0");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.75");
                    output.WriteLine("          reform_desire = 1.05");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.75");
                    output.WriteLine("          reform_desire = 1.10");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.66");
                    output.WriteLine("          reform_desire = 1.15");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.50");
                    output.WriteLine("          reform_desire = 1.20");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.25");
                    output.WriteLine("          reform_desire = 1.25");
                    output.WriteLine("      }");
                    output.WriteLine("      modifer = {");
                    output.WriteLine("          factor = 0.1");
                    output.WriteLine("          NOT = { religion_years = { " + s + " = 12 } }");
                    output.WriteLine("      }");
                    output.WriteLine("  }");
                    output.WriteLine("  option = {");
                    output.WriteLine("      name = " + '"' + name + ".EVE" + CurrentID + '"');
                    output.WriteLine("      change_religion = " + s);
                    output.WriteLine("  }");
                    output.WriteLine("}");
                    output.Flush();

                    //localize the event:
                    localization.WriteLine(" " + name + ".NAME:" + CurrentID + '"' + "Spread of " + s + '"');
                    localization.WriteLine(" " + name + ".DESC:" + CurrentID + '"' + s + " has spread into your province of $PROVINCENAME$" + '"');
                    localization.Flush();

                    CurrentID++;
                }
            }

        }

        /// <summary>
        /// creates the icons, however, is current BROKEN. 
        /// </summary>
        protected void CreateRelgiousIcons()
        {
            MagickImageCollection newIconReligion = new MagickImageCollection();
            MagickImage iconReligion = new MagickImage(Constants.INSTALLPATH + "/gfx/interface/icon_religion.dds");
            newIconReligion.Add(iconReligion);

            Bitmap empty = new Bitmap(64, 64);

            foreach (List<string> list in religions)
            {
                foreach (string s in list)
                {

                    Console.WriteLine("Adding in religious icon for " + s);
                    newIconReligion.Add(new MagickImage(empty));
                }
            }
            
            using (MagickImage result = newIconReligion.Mosaic())
            {
                result.Format = MagickFormat.Dds;
                //does not work. Magick seems to remove the extra blank space. 
                result.Write(Constants.GFX + "/interface/icon_religion.dds");
                result.Write(Constants.GFX + "/interface/icon_religion.png");
            }

        }
    }
}
